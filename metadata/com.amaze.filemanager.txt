Categories:Office
License:GPLv3
Web Site:
Source Code:https://github.com/arpitkh96/AmazeFileManager
Issue Tracker:https://github.com/arpitkh96/AmazeFileManager/issues

Auto Name:Amaze File Manager
Summary:File Manager
Description:
Light and smooth file manager following the Material Design
guidelines.

Features:

- Basic features like cut, copy, delete, compress, extract etc. easily accessible
- Work on multiple tabs at same time
- Multiple themes with cool icons
- Navigation drawer for quick navigation
- App Manager to open, backup, or directly uninstall any app
- Quickly access history, access bookmarks or search for any file
- Root explorer for advanced users
.

Repo Type:git
Repo:https://github.com/arpitkh96/AmazeFileManager

Build:1.3.0,4
    disable=ucmfix
    commit=ucmfix

Build:1.3.1,5
    commit=361b80e16ac0e151a69a8f0c3c9bb65de2a74ad1
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/RootTools.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/ && \
        sed -i -e "/apply plugin: 'android'/d" build.gradle

Build:1.4.0,6
    commit=v1.4
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Build:1.4.2,8
    disable=re-release by upstream
    commit=v1.4.2
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.4.2
Current Version Code:8

