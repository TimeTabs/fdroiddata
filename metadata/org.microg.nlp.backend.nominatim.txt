Categories:Navigation
License:Apache2
Web Site:https://github.com/microg/android_packages_apps_UnifiedNlp
Source Code:https://github.com/microg/NominatimGeocoderBackend
Issue Tracker:https://github.com/microg/NominatimGeocoderBackend/issues

Name:NominatimNlpBackend
Auto Name:NominatimGeocoderBackend
Summary:UnifiedNlp geocoding provider (MapQuest Nominatim)
Description:
[[com.google.android.gms]] backend that uses MapQuest's Nominatim service
(based on OpenStreeMap) for geocoding.

[https://github.com/microg/NominatimGeocoderBackend/releases Changelog]
.

Repo Type:git
Repo:https://github.com/microg/NominatimGeocoderBackend

Build:1.0.0,1000
    commit=v1.0
    srclibs=1:UnifiedNlpApi@v1.1.2
    target=android-21

Build:1.1.0,1100
    commit=v1.1.0
    gradle=yes
    submodules=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.0
Current Version Code:1100

