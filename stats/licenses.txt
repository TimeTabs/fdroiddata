GPL 26
PublicDomain 10
GNUFDL 1
GPLv3+ 137
GPLv2 97
GPLv3 586
GPL/MPL 1
AGPLv3 10
NYSLv0.9982 1
EPL 1
ISC 6
NetBSD 1
GPL/Artistic 2
AGPL 6
MIT 132
CCBYSA 1
WTFPL 12
GNUFDL/CCBYSA 1
Missing 1
Fair License 1
FreeBSD 6
BSD 4
Beer License 2
GPLv2+ 50
Unlicense/Apache2 1
NetBSD/Apache2 1
Zlib 2
MPL2 10
GPLv3 or New BSD 1
EUPL 1
X11 1
Apache2 362
LGPLv2.1 2
LGPL 12
NewBSD 30
Apache2,CC-BY-SA 1
MPL 1
MPL1.1 1
Artistic2 3
CC0 1
Unlicense 2
AGPLv3+ 7
BSD/Apache 1
Expat 1
LGPLv3 1
NCSA 1
